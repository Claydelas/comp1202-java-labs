import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FlashCardTest {

	@Test
	void test() {
		String q = "Question";
		String a = "Answer";
		
		FlashCard fc = new FlashCard(q, a);
		
		assertEquals(fc.getQuestion(), q, "getQuestion returns the question on the flash card");
		assertEquals(fc.getAnswer(), a, "getAnswer returns the answer on the flash card");
	}

}
