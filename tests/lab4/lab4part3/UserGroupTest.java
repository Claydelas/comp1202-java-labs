import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class UserGroupTest {

	UserGroup users;
	
	@BeforeEach
	public void setup() {
		users = new UserGroup();
		
		String[] people = {"Kevin Rowe", "Jack Daniels", "Barry Smith", "Hugh Davies", "Pete Jackson", "Jerry Simpson", "Teresa Szelankovic", "Brian Degrasse Tyson", "Mike Hardcastle", "Danny Hanson"};
		for (int i=0; i<10; i++) {
		  users.getUsers().add(new User(Integer.toString(i), "user", people[i]));
		}
	}
	
	@Test
	@DisplayName("Tests the removeFirstUser method")
	void testGetUser() {	
		User firstUser = users.getUser(0);
		users.removeFirstUser();
		assertFalse(users.getUsers().contains(firstUser), "Testing that the first user was removed");
		
	}
	
	@Test
	@DisplayName("Tests the removeLastUser method")
	void testGetUser2() {
		User lastUser = users.getUser(users.getUsers().size()-1);
		users.removeLastUser();
		assertFalse(users.getUsers().contains(lastUser), "Testing that the last user was removed");
		
	}

	@Test
	@DisplayName("Tests the removeUser method")
	void testGetUser3() {
		User newFirstUser = users.getUser(0);
		String newFirstUserName = users.getUser(0).getUsername();
		users.removeUser(newFirstUserName);
		
		assertFalse(users.getUsers().contains(newFirstUser), "Testing that the new first user was removed");
		
	}
	
}
