import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.sampled.BooleanControl.Type;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class CalculatorTest {

	@Test
	@DisplayName("Check the number of methods")
	void methodTest() {
		Calculator cal = new Calculator();
		Method[] methods = cal.getClass().getDeclaredMethods();
		
		
		ArrayList<String> types = new ArrayList<String> ();
		
		for(int i = 0; i<methods.length; i++) {
			assertEquals(methods[0].getName().toString(), "x", "Check that there are no new method names");
			types.add(methods[i].getParameterTypes()[0].toString());
			System.out.println(methods[i].getParameterTypes()[0].toString());
		}
		
		assertEquals(methods.length, 3, "Check that there are only three methods");
		
		assertEquals(types.size(), 3, "Checks correct number of parameters");
		
		assertTrue(types.contains("double"), "Checks there is a double parameter");
		assertTrue(types.contains("class java.lang.Double"), "Checks there is a Double parameter");
		assertTrue(types.contains("class java.lang.String"), "Checks there is a String parameter");
		
	}

	@Test
	@DisplayName("Test adding")
	void addingTest() {
		Calculator cal = new Calculator();
		assertEquals(cal.x("12 + 5"), new Double(17));
	}
	
	@Test
	@DisplayName("Test negative adding")
	void negativeAddingTest() {
		Calculator cal = new Calculator();
		assertEquals(cal.x("5 + 2"), new Double(7));
		assertEquals(cal.x("-1 + -2"), new Double(-3));
	}
	
	@Test
	@DisplayName("Test multiplication")
	void multiplicationTest() {
		Calculator cal = new Calculator();
		assertEquals(cal.x("12 * 5"), new Double(60));
	}

	@Test
	@DisplayName("Test negative multiplication")
	void negativeMultiplicationTest() {
		Calculator cal = new Calculator();
		assertEquals(cal.x("5 * 3"), new Double(15));
		assertEquals(cal.x("-5 * -3"), new Double(15));
	}
	
	@Test
	@DisplayName("Check the functionality of the calculator is correct")
	void malformedTest() {
		Calculator cal = new Calculator();
		assertEquals(cal.x("12 [ 3"), null);
	}
	
}